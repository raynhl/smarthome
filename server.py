from flask import Flask, request, jsonify, Response, send_from_directory
import importlib
import json
import threading
import struct
import binascii

app = Flask(__name__)

device_configs = json.load(open("device-configs.dat"))
devices = dict()

for device_config in device_configs["devices"]:
	device_module = device_config["device_module"].split(".")[0]
	module = importlib.import_module("Devices.{}".format(device_module))
	device_class = getattr(module, device_module)
	
	obj = device_class(device_config)
	obj.print()
	devices.update({device_config["device_id"] : obj})

@app.route('/')
def hello_world():
	return "Ray says Hello World!"

@app.route("/<device_id>/update-status/", methods=["POST"])
def update_status(device_id):
	if request.method == "POST":
		data = request.get_json(silent=True)
		
		if device_id == data["device_id"]:
			data["status"] = convert_to_status_code(data)
			status_code = devices[device_id].update(data)
			return Response(status = status_code)

		else:
			print("[ERROR] Device IDs mismatch!")
			return Response(status = 404)

@app.route("/<device_id>/delayed/update-status/", methods=["POST"])
def delayed_update_status(device_id):
	if request.method == "POST":
		data = request.get_json(silent=True)

		if device_id == data["device_id"]:
			data["status"] = convert_to_status_code(data)
			action_thread = threading.Thread(target = devices[device_id].delayed_update, args = (data, ))
			action_thread.daemon = True
			action_thread.start()
			return Response(status = 200)

		else:
			print("[ERROR] Device IDs mismatch!")
			return Response(status = 404)

@app.route("/<device_id>/timed/update-status/", methods=["POST"])
def timed_update_status(device_id):
	if request.method == "POST":
		data = request.get_json(silent=True)

		if device_id == data["device_id"]:
			data["status"] = convert_to_status_code(data)
			action_thread = threading.Thread(target = devices[device_id].timed_update, args = (data, ))
			action_thread.daemon = True
			action_thread.start()
			return Response(status = 200)

		else:
			print("[ERROR] Device IDs mismatch!")
			return Response(status = 404)

@app.route("/fota/<vendor_id>/<device_id>/check")
def fota_version_check(vendor_id, device_id):
	device = devices[device_id]

	obj_vendor_id = int(binascii.hexlify(device.vendor_id.encode("utf-8")), 16)
	obj_device_id = int(binascii.hexlify(device.device_id.encode("utf-8")), 16)
	obj_major_version = int(device.major_version, 16)
	obj_minor_version = int(device.minor_version, 16)

	header = struct.pack("<IIBB", obj_vendor_id, obj_device_id, obj_major_version, obj_minor_version)
	print(header)
	return header

@app.route("/fota/<vendor_id>/<device_id>/image")
def fota_image_download(vendor_id, device_id):
	device = devices[device_id]
	filename = "{}-{}-{}-{}.bin".format(vendor_id, device_id, device.major_version, device.minor_version)
	return send_from_directory(directory = "FW_Images", filename = filename)

@app.route("/heartbeat/<vendor_id>/<device_id>/<major_version>/<minor_version>/<current_state>")
def update_heartbeat(vendor_id, device_id, major_version, minor_version, current_state):
	major_version = major_version.upper()
	minor_version = minor_version.upper()
	current_state = current_state.upper()
	print("[HEARTBEAT]| {} | {} | v{}.{} | Status: 0x{} |".format(vendor_id, device_id, major_version, minor_version, current_state))
	
	obj = devices[device_id]
	obj.current_state = int(current_state,16)
	obj.major_version = major_version
	obj.minor_version = minor_version
	
	return Response(status = 200)

def convert_to_status_code(data):
	if "on" in data["status"]:
			return 1
	else: return 0


app.run(host = "0.0.0.0",
		debug = True)