#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ArduinoJson.h>

#define VENDOR_ID						0x5F524159        // "_RAY"
#define VENDOR_ID_STR					"_RAY"
#define DEVICE_ID						0x524C5938      // "RLY8"
#define DEVICE_ID_STR					"RLY8"
#define FW_MAJOR_VERSION				0x01
#define FW_MINOR_VERSION				0x01
#define SERVER_BASE_URL					"http://<server_ip_addr>:<port>/"
#define SERVER_HEARTBEAT_URL			SERVER_BASE_URL"heartbeat/"VENDOR_ID_STR"/"DEVICE_ID_STR"/"
#define BIT_MASK						0x01

#define RELAY_0   14
#define RELAY_1   12
#define RELAY_2   13
#define RELAY_3   15
#define RELAY_4   0
#define RELAY_5   16
#define RELAY_6   2
#define RELAY_7   5

void handleUpdateStatus();
void handleFotaUpdate();

const char* ssid				= "MushyPeas";
const char* password			= "accessnetwork";
const long 	heartBeatInterval 	= 10000;

const uint8_t RELAYS[] = {
	RELAY_0,
	RELAY_1,
	RELAY_2,
	RELAY_3,
	RELAY_4,
	RELAY_5,
	RELAY_6,
	RELAY_7
};

const IPAddress ip(192,168,10,151);   
const IPAddress gateway(192,168,10,1);   
const IPAddress subnet(255,255,255,0); 

ESP8266WebServer server(80);

unsigned long 	currentMillis 	= 0;
unsigned long 	previousMillis 	= 0;
uint8_t 		currentState 	= 0x00;

struct FotaHeader {
	uint32_t	vendorId;
	uint32_t	deviceId;
	uint8_t		majorVersion;
	uint8_t		minorVersion;
};



void setup() {
	Serial.begin(9600);
	delay(100);

	// We start by connecting to a WiFi network
	Serial.println();
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);

	WiFi.begin(ssid, password);
	WiFi.config(ip, gateway, subnet);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");  
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

	server.on("/update", handleUpdateStatus);
	server.on("/fota_update", handleFotaUpdate);
	server.begin();
	Serial.println("Server listening");
	printDeviceInfo();

	initializeGpios();
	delay(100);
	turnOffAllRelays();
}

void loop() {
	server.handleClient();
	currentMillis = millis();

	if (currentMillis - previousMillis >= heartBeatInterval) {
		previousMillis = currentMillis;
		sendHeartBeat();
	}
}

void handleUpdateStatus() {
	Serial.println("Updating Status");

	StaticJsonBuffer<150> JSONbuffer;
	JsonObject& parsed = JSONbuffer.parseObject(server.arg(0));
	
	if (!parsed.success()) {
		Serial.println("Parsing failed");
		server.send(404, "text/plain", "");
	}

	const char *relayStatusPtr = parsed["status"];
	uint8_t relayStatus = String(relayStatusPtr).toInt();
	Serial.print("Status: ");
	Serial.println(relayStatus, BIN);
	
	handleRelayState(relayStatus);

	server.send(200, "text/plain", "");
}

void handleFotaUpdate() {
	server.send(200, "text/plain", "");
	checkForUpdates();
}

void checkForUpdates() {
	String fwVersionURL = String(SERVER_BASE_URL);
	fwVersionURL.concat("fota/");
	fwVersionURL.concat(String(VENDOR_ID_STR));
	fwVersionURL.concat("/");
	fwVersionURL.concat(String(DEVICE_ID_STR));
	fwVersionURL.concat("/check");
	Serial.println(fwVersionURL);

	HTTPClient httpClient;
	httpClient.begin(fwVersionURL);

	if (httpClient.GET() == 200) {
		String payload = httpClient.getString();
		byte payloadBuffer[sizeof(FotaHeader)];
		payload.getBytes(payloadBuffer, sizeof(FotaHeader));
		struct FotaHeader* fotaHeader = (FotaHeader*) &payloadBuffer;

		if (matchVendorIdAndDeviceId(fotaHeader) && needFwUpdate(fotaHeader)) {
			Serial.println("New FW version available. Starting update.");
			performUpdate();
		}

		if (matchVendorIdAndDeviceId(fotaHeader)) {
			Serial.println("Already running on latest FW version."); 
		}

		else {
			Serial.println("Vendor ID and/or Device ID mismatch. Check if firmware is for this device.");
		}
	}

	httpClient.end();
}

void performUpdate() {
	String fwImageURL = String(SERVER_BASE_URL);
	fwImageURL.concat("fota/");
	fwImageURL.concat(String(VENDOR_ID_STR));
	fwImageURL.concat("/");
	fwImageURL.concat(String(DEVICE_ID_STR));
	fwImageURL.concat("/image");
	Serial.println(fwImageURL);

	// shutdown device function
	turnOffAllRelays();

	t_httpUpdate_return ret = ESPhttpUpdate.update(fwImageURL);

	switch(ret) {
		case HTTP_UPDATE_FAILED:
		Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
		break;

		case HTTP_UPDATE_NO_UPDATES:
		Serial.println("HTTP_UPDATE_NO_UPDATES");
		break;
	}
}

bool matchVendorIdAndDeviceId(FotaHeader* fotaHeader) {
	return (fotaHeader->vendorId == VENDOR_ID) && (fotaHeader->deviceId == DEVICE_ID);
}

bool needFwUpdate(FotaHeader* fotaHeader) {
	return (fotaHeader->minorVersion > FW_MINOR_VERSION) || (fotaHeader->majorVersion > FW_MAJOR_VERSION);
}

void handleRelayState(uint8_t relayStatus) {
	for (uint8_t i = 0; i < 8; i++) {
		if (((relayStatus >> i) & BIT_MASK) != ((currentState >> i) & BIT_MASK)) {
			if (((relayStatus >> i) & BIT_MASK) == 1) {
				turnOnRelayByIndex(i);
			}

			else {
				turnOffRelayByIndex(i);
			}
		}
	}
}

void initializeGpios() {
	for (uint8_t i = 0; i < 8; i++) {
		pinMode(RELAYS[i], OUTPUT);
	}
}

void turnOnRelayByIndex(uint8_t index) {
	String message = "Switched on Relay ";
	message.concat(index);
	message.concat(".");
	Serial.println(message);
	
	digitalWrite(RELAYS[index], LOW);
	currentState = currentState ^ (BIT_MASK << index);
}

void turnOffRelayByIndex(uint8_t index) {
	
	String message = "Switched off Relay ";
	message.concat(index);
	message.concat(".");
	Serial.println(message);

	digitalWrite(RELAYS[index], HIGH);
	currentState = currentState & ~(BIT_MASK << index);
}

void turnOffAllRelays() {
	for (uint8_t i = 0; i < 8; i++) {
		turnOffRelayByIndex(i);
	}
}

void printDeviceInfo() {
	Serial.print("[INFO] I am: <");
	Serial.print(VENDOR_ID_STR);
	Serial.print(" : ");
	Serial.print(DEVICE_ID_STR);
	Serial.println(">");
	printFwVersion();
}

void printFwVersion() {
	Serial.print("[INFO] Running on FW v");
	Serial.print(FW_MAJOR_VERSION, HEX);
	Serial.print(".");
	Serial.println(FW_MINOR_VERSION, HEX);
}

void sendHeartBeat() {
	String heartBeatURL = String(SERVER_HEARTBEAT_URL);
	heartBeatURL.concat(String(FW_MAJOR_VERSION,HEX));
	heartBeatURL.concat("/");
	heartBeatURL.concat(String(FW_MINOR_VERSION,HEX));
	heartBeatURL.concat("/");
	heartBeatURL.concat(String(currentState,HEX));
	Serial.println(heartBeatURL);
  
	HTTPClient httpClient;
	httpClient.begin(heartBeatURL);

	if (httpClient.GET() == 200) {
		Serial.println("Heartbeat update successful.");
	}

	else {
		Serial.println("Heartbeat update failed.");
	}
}